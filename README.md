# Research
    
*  Asset allocation and portfolio risk management consulting for hedge funds and mutual funds (2006-2008, Lehman Brothers) 
*  Trading strategies and liquidity management for hedge funds and mutual funds (2006-2008, Lehman Brothers) 
* Hong Y, Kim SH and Kang H. 2012. Does pair trading work in Korean market? Actual Problems of Economics, January, 127(1): 454-462. (correspondence) 
* Baik B, Kang H, Kim Y. 2013. Volatility Arbitrage around Earnings Announcements: Evidence from the Korean Equity Linked Warrants Market. Pacific-Basin Finance Journal. 29:109-130. (correspondence) 
* Kim SH, Kang H. 2014. A New Strategy using Term-structure Dynamics of Commodity Futures. Financial Research Letters. 11(3): 282-288. (Corresponding author). 
* Kim SH, Kang HG. 2015. Tactical asset allocation using investors’ sentiment. Hitotsubashi Journal of Economics. 56 (2): 177-196. Corresponding author. 
* Kim YH, Kang HG, Lee JK. 2017. Can big data forecast North Korean military aggression? Defence and Peace Economics. 1-18. 
* Hawn, O. and Kang, H. (2018). The Effect of Market and Nonmarket Competition on Firm and Industry Corporate Social Responsibility, In Sustainability, Stakeholder Governance, and Corporate Social Responsibility (Advances in Strategic Management, Vol. 38), Emerald Publishing Limited, 313-337. https://doi.org/10.1108/S0742-332220180000038017 (Corresponding) 
* Kwark NK, Jun SG, Kang HG. 2015. Can Derivatives Information Predict Stock Price Jumps? Journal of Applied Business Research. 31(3): 845-860.
* Rhee DW, Kang HG, Kim SH. 2015. Strategic Asset Allocation of Credit Guarantors Journal of Applied Business Research. 31(5): 1823-1834. Corresponding author. 
* Kang HG & Kim SH. 2018. Order Imbalance and Return Predictability: Evidence from Korean Index Futures. INFORMATION - An International Interdisciplinary Journal. 21(4): 1283-1291. 
* Kim Soo-Hyun & Kang, H. 2018. Modeling time-varying variability of asset returns with entropy. INFORMATION - An International Interdisciplinary Journal. 21(4): 1321-1332. Corresponding author. "This work was supported by the Ministry of Education of the Republic of Korea and the National Research Foundation of Korea (NRF-2017S1A5A8018885)" 
* Kang, H. & Kang, C. 2009. The effect of credit risk on stock returns. Journal of Economic Research. 14 (2). 49 -- 67. (correspondence) 
* Kang, H. 2009. Time-varying macroeconomic exposures in the Indian stock market. Journal of South Asian Studies. 15 (1). 209 -- 238. (single) 
* Kang, H. 2010. Dynamic asset allocation under mispricing, predictability and portable alpha. International Studies Review. 11 (1). 75 -- 102. (single)
* Lee, C. & Kang, H. 2010. The joint dynamics of stock and bond risk-returns in Japan. International Studies Review. 11 (2). 93 -- 100. (correspondence)
* Lee C. & Kang H. 2011. Success Strategies for ETF and the Case of Barclays Global Investors, POSRI Business and Economic Review. 11(3): 215-230. (correspondence) ETF 성공 전략과 Barclays Global Investors의 사례, POSRI 경영경제연구 . 
* Yi D, Kang H, Kim S, Lee C. 2013. Autocorrelation Analysis of the Sentiment with Stock Information Appearing on Big-Data. The Korean Journal of Financial Engineering. 12(2): 79-96. 금융공학연구. 빅데이터에 나타난 감성 분석. 공동저자. 
* Jun H, Woo W, Kang H. 2013. How to succeed in the presence of financial and technological gaps: from the perspective of governance innovation. International Studies Review. 14(2):1-16. 
* Lee C, Kang H, Yi YS. 2013. Growth Strategies at Foreign-Invested Enterprises through Stabilizing Industrial Relations - A Case Study on Oriental Brewery. International Studies Review. 14(2): 35-58. (Correspondence) This work was supported by the National Research Foundation of Korea Grant funded by the Korean Government (NRF-2013S1A5A8021046) 
* 강형구, 이창민, 최한수, 한민연. 2014. 개별 주식 수익률 변동성의 결정요인. 재무관리연구. 31(1):145-172. The Determinants of Heterogeneous Volatility Process. The Korean Journal of Financial Management. This work was supported by the research fund of Hanyang University (HY-2013). ● Lee DG, Kim JB, Kang HG. 2014. Do larger brokerage firms enjoy larger economies of scale and scope? Seoul Journal of Economics. 27(4): 445-467. 
* Jeong HJ, Kang HG. 2015. No Low Volatility Effect In Low Volatility Market? Journal of the Korean Data Analysis Society. 17(3): 1225-1233. This work was supported by the research fund of Hanyang University (HY-2014). 
* Kim SH, Lee K, Kang HG. 2015. Leveraged/Inverse ETFs and Volatility in the Korean Market. Korean Journal of Futures and Options. 23(3): 323-336. This work was supported by the research fund of Hanyang University (HY-2014). 
* 임은아. 강형구. 전상경. 2016. 국민연금의 외환거래가국내 외환시장에 미치는 영향. 선물연구. 24(3): 399-421.
* 한민연, 강형구 (2017). 국내 주식시장에서의 요인 수익률과 그 위험에 대한 연구. 대한경영학회지. 30(4): 631-656.(HY-2015년도) (교신저자)
* Han BS, Jun SG, Kim EJ, Kang HG (2017). A Case Study of Option Shock in the Korean Stock Market. Korea Business Review. 21(4): 99-117 (한병석, 전상경, 김은지, 강형구. 2017. 한국주식 시장의 옵션쇼크 사례연구. 한국경영학회. (correspondence) 
* 강형구, 전성민. (2018). 국내 전자 상거래의 규제 및 글로벌 경쟁 이슈: 시장지배력, 데이터 주권, 아마존 효과를 중심으로. 법경제학연구. 15(3): 355-373. 이 논문 또는 저서는 2018년 대한민국 교육부와 한국연구재단의 지원을 받아 수행된 연구임(NRF-2018S1A5A8027076) This work was supported by the Ministry of Education of the Republic of Korea and the National Research Foundation of Korea(NRF-2018S1A5A8027076)
* 강형구, 배경훈, & 구본하. (2019). 인공지능과 금융투자 전략. 한국경제포럼, 12(3), 93-110. 
* 알고리즘 트레이딩이 금융시장에 미치는 영향에 대한 분석, 한양대학교, 책임연구원 
* 가계부채, 주택가격조정, 기업구조조정 등에 대한 조기경보 및 정책대응 등을 위한 빅데이터 활용 방안, 한국은행, 책임연구원
* investors use options and futures to trade on different types of information? Evidence from an aggregate stock index.& joint with Dixon, I, 38, no.2 (2018): 175-198.
* Liquidity risk and Exchange-traded-fund returns, variances, and tracking errors& joint with Kim. (Forthcoming at the Journal of Financial Economics) 
* 제33회 경제금융대학 학술제 준비 위원회 (2017), “다가오는 현금의 종말과 정부의 역할,” 경제금융대학 학술지, 33, 7-51. 
* 이웅석, 양성택, 류호영, 이송현 (2018), “KRX 시장 통합형 신규지수 개발방안 : KOSDAQ 시장 활성화 방안을 중점으로,” KRX Market, 2018(2), 66-108.
* 강형구, 배경훈, 양성택, & 최창희. (2019). 생애주기와 경기변동주기를 이용한 투자전략. 한국증권학회지, 48(6), 721-754. [pdf](papers_file/%EC%83%9D%EC%95%A0%EC%A3%BC%EA%B8%B0%EC%99%80_%EA%B2%BD%EA%B8%B0%EB%B3%80%EB%8F%99%EC%A3%BC%EA%B8%B0%EB%A5%BC_%EC%9D%B4%EC%9A%A9%ED%95%9C_%ED%88%AC%EC%9E%90%EC%A0%84%EB%9E%B5.pdf)
* Hyoung-goo Kang, Dong-hyun Lee, Minyeon Han. (2020). Market Anomalies in the Korean Stock Market. Korean Journal of Futures and Options, 28(2), 159-228. [pdf](https://gitlab.com/pr_handa/research/-/blob/master/papers_file/Korean_market_anomalies.pdf)

# Patent & Technology
## [특허등록] 투자 포트폴리오를 구축하는 장치 및 방법 
    ○ 등록번호 : 제 10-1954933호 
    ○ 출원번호 : 제 10 - 2018 - 0079003호 
    ○ 출원일 : 2018년 07월 06일 
    ○ 등록일 : 2019년 02월 27일 
## [특허등록] 빅데이터 및 인공지능을 활용한 이벤트 예측 시스템 및 그 방법 
    ○ 등록번호 : 제 10-2000663 호
    ○ 출원번호 : 제 10-2018-0036764 호 
    ○ 출원일 : 2018년 03월 29일
    ○ 등록일 : 2019년 10월 01일 
## [특허등록] 오토인코더를 이용한 산업분류 시스템 및 방법 
    ○ 등록번호 : 제 10-2041242 호 
    ○ 출원번호 : 제 10 - 2018 - 0036433 호 
    ○ 출원일 : 2018년 03월 29일 
    ○ 등록일 : 2019년 10월 31일3. 기술임치
## [특허출원] 투자 포트폴리오를 구축하는 장치 및 방법 
    ○ 출원번호 :  제 10 - 2018 - 0122757 호 
    ○ 출원일 : 2018년 10월 15일
## [기술임치] 글로벌 리스크 경보와 관리를 위한 시스템 개발 
    ○ 발급번호 : 20200128091800004 
    ○ 계약번호 : 2020-02-60-1416 
    ○ 개발인 : 주식회사한다파트너스 
    ○ 개발인(공동) : 한양대학교 산학협력단 
    ○ 임치기간 : 2020년 01월 23일 ~ 2021년 01월 22일
## [기술이전] Environmental, Social and Corporate Governance 분석 기술 
    ○ 양도인: 한양대학교 산학협력단 
    ○ 양수인: 주식회사한다파트너스 
    ○ 계약체결: 2020년 02월 28일