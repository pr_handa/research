*This data is for research purposes only and is not for commercial use.  
Also, data can be changed, not available or terminated at any time.  
The resulting disadvantages in use are entirely for the user.  
If you have any questions, please feel free to contact us.  
info@handapartners.com*
# asset pricing factor model research data
company universe: KOSPI or KOSPI + KOSDAQ common stock  
excluded: financial company, capital erosion  
delisting: -100% return on next business day  

- [fama french 3 factor in korea daily return (only kospi)](https://drive.google.com/open?id=1y3t2-1_gz9dzuxFtGI2rsLwTOHFZIKke)
- [fama french 3 factor in korea daily return (kospi & kosdaq)](https://drive.google.com/open?id=1xg0dCIVVkdMzKKEg6Acc3NPaHaaDIfns)
- [fama french 5 factor in korea daily return (only kospi)](https://drive.google.com/open?id=1y-OxvF1u_JDza5wu93KsekM5YgEG3npO)
- [fama french 5 factor in korea daily return (kospi & kosdaq)](https://drive.google.com/open?id=1xg-b7P1QjZ8-ow7qQCENWqMu9U1LTsgL)
___
- [fama french research portfolio (2 * 3) (only kospi)](https://drive.google.com/open?id=1y-EaBacfx5JCvHO6zOtGJIud7TDjEczg)
- [fama french research portfolio (2 * 3) (kospi & kosdaq)](https://drive.google.com/open?id=1xYiGzRdLxKrmd4CL9D6KOyT0KxxBtPOR)
___
- [What is a specific company's portfolio? (only kospi)](https://drive.google.com/open?id=1xnUFTBVx7T24Svi2G6Zw9MHYnQKEKKRQ)
- [What is a specific company's portfolio? (kospi & kosdaq)](https://drive.google.com/open?id=1xUodhvY4JMRuPa08P5Evg9Rq9PvXvZvt)
